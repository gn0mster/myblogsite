dragNdrop.js
//   I made this in a scripting college class and I'm tweaking for my site

// Declare variables
    var diffX, diffY, theElement;
       
//**********************************************************************************
//   The event handler function for grabbing the word 
    function grabber(event) {

//   Set the global variable for the element to be moved
    theElement = event.currentTarget;
    
//   Determine the position of the word to be grabbed,
//   first removing the units from left and top
    var posX = parseInt (theElement.style.left);
    var posY = parseInt (theElement.style.top);
    
//   Computer the difference between where it is and 
//   where the mouse click occurred
    diffX = event.clientX - posX;
    diffY = event.clientY - posY;
    
//   Now register the event handlers for moving and 
//   dropping the word
    document.addEventListener ("mousemove", mover, true);
    document.addEventListener ("mouseup", dropper, true);
    
//   Stop propagation of the event and stop any default
//   browser action
    event.stopPropagation();
    event.preventDefault();
    
}  //** end of gabber

//***********************************************************************************
//  The event handler function for moving the word
   function mover(event) {
   
//  Computer the new position, add the units, and move the word
   theElement.style.left= (event.clientX - diffX) + "px";
   theElement.style.top= (event.clientY - diffY) + "px";
   
//  Prevent propagation of the event
   event.stopPropagtion();
}   //** end of mover

//***********************************************************************************
//  The event handler function for dropping the word 
   function dropper(event) {

//  Unregister the event handlers for mouseup and mousemove
   document.removeEventListener("mouseup", dropper, true);
   document.removeEventListener("mousemove", mover, true);
   
//  Prevent propagation of the event
   event.stopPropagation();
}  //** end of dropper