package net.seenaomi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import see.naomi.beans.BlogPost;

/**
 * Servlet implementation class Dispatcher
 */
@WebServlet("/Dispatcher")
public class Dispatcher extends HttpServlet {
	private static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Dispatcher() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("start doPost");
		String action = request.getParameter("action");
		if (action != null && action.equals("create")) {
			Connection conn = null;
			PreparedStatement stmt = null;
			try {
				conn = DriverManager.getConnection("jdbc:mysql://localhost/MyBlogSiteDB?user=root&password=");
				stmt = conn.prepareStatement("INSERT INTO BlogPost (created, title, content) VALUES(?,?,?)");
				stmt.setDate(1, new java.sql.Date(new java.util.Date().getTime()));
				stmt.setString(2, request.getParameter("title"));
				stmt.setString(3, request.getParameter("content"));
				stmt.execute();
				int count = stmt.getUpdateCount();	//should be 1
				if (count != 1) {
					//error, insert didn't happen
					reportErrorToUser(response, "BlogPost could not be created, please try again later (yeah right)");
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				//error, insert didn't happen
				try {
					reportErrorToUser(response, "BlogPost could not be created, please try again later (yeah right)");
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			finally {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
//			getRecentBlogPosts(request,response);
		}
		else {
			getRecentBlogPosts(request, response);
		}
	}

	private void getRecentBlogPosts(HttpServletRequest request, HttpServletResponse response) {
		String orderBy = request.getParameter("orderBy");
		String order = "ORDER BY created DESC";
		if (orderBy.equalsIgnoreCase("comments")) {
			order = "ORDER BY numberOfComments DESC";
		}
		PrintWriter writer = null;
		StringBuilder builder = new StringBuilder(500);
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/MyBlogSiteDB?user=root&password=");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM BlogPost "+order);
			List<BlogPost> list = BlogPost.createBlogPost(rs);
			builder.append("[");
			Iterator<BlogPost> it = list.iterator();
			while(it.hasNext()) {
				BlogPost post = it.next();
				builder.append("{");
				builder.append("\"title\": \""+post.getTitle()+"\",");
				builder.append("\"content\": \""+post.getContent()+"\"");
				builder.append("}");
				if (it.hasNext()) {
					builder.append(",");
				}
			}
			builder.append("]");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		try {
			writer = response.getWriter();
			writer.write(builder.toString());
		}
		catch (Exception e) {
			logger.error("Error processing data",e);
			if (writer != null) {
				builder.setLength(0);
				builder.append("{");
				builder.append("\"status\": \"error\",");
				builder.append("\"msg\": ");
				builder.append(e.getMessage());
				builder.append("}");
				try {
					writer.write(builder.toString());
				}
				catch (Exception e2) {
					logger.error("Error processing error",e);
				}
			}
		}
		finally {
			if (writer != null) {
				writer.close();
			}
		}
		
	}
	private void reportErrorToUser(HttpServletResponse response, String message) throws Exception {
		PrintWriter writer = response.getWriter();
		StringBuilder builder = new StringBuilder(500);
		builder.append("{");
		builder.append("\"status\": \"error\",");
		builder.append("\"message\": ");
		builder.append("\""+message+"\"");
		builder.append("}");
		System.out.println("error: "+builder.toString());
		try {
			writer.write(builder.toString());
		}
		catch (Exception e2) {
			logger.error("Error processing error",e2);
		}
		finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.error("Could not load JDBC driver",e);
			throw new ServletException(e);
		}
	}

}
