package net.seenaomi.test;

public class TestProject {
	private int counter = 0; //private scope most vars are private ints are 32bits wide - scaler data type
	long counter2 = 100000000000000000L; //protected scope every var has a specific type longs are 64bits - scaler data type	
	private boolean flag = true; //boolean also a scaler type - if I don't set a value it will default to false
	private float distance = (float)1.2; //float is also a scaler type - this is an example of a cast - defaults to zero
	private double bigDistance = 1.2; //double have twice as much precision than floats - doubles have decimals - defaults to zero
	//scalers are not classes they are primitives they can not be null
	protected Integer iCounter = null; //not primitive is a class - if I don't set a value it will default to null
	private Long lCounter2 = counter2; //not primitive is a class
	private TestProject me; //we can use our own classes - classes are inner changeable - this var is very different from the class - vars hold values classes do not they are just a definition
	private static TestProject singleton; //static are shared amongst the class
	
	
	public TestProject() { //this is a default constructor because of no parameters 
		flag = true;
		incrementCounter();
	}
	
	public int getCounter() { //to create get/setters of all vars right click go to source generate getters and setters
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter; 
	}

	public long getCounter2() { //most methods are public
		return counter2;
	}

	public void setCounter2(long counter2) { //setting a method private isolates the method to that class and no other class can call it
		this.counter2 = counter2;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public double getBigDistance() {
		return bigDistance;
	}

	public void setBigDistance(double bigDistance) {
		this.bigDistance = bigDistance;
	}

	public Integer getiCounter() {
		return iCounter;
	}
	
	public void incrementCounter() {
		iCounter++;
	}
	
	public Long getlCounter2() {
		return lCounter2;
	}

	public void setlCounter2(Long lCounter2) {
		this.lCounter2 = lCounter2;
	}

	public TestProject getMe() {
		return me;
	}

	public void setMe(TestProject me) {
		this.me = me;
	}

	public TestProject(float distance) { //this is a constructor
		
	}
	
	public TestProject makeMe(){ //each copy of a constructor owns its own copy of the variables unless you define static then there is only 1
		TestProject tp = new TestProject(); //new equals new copy - local vars never have scope
		System.out.println(tp.flag); //debugging tool in Java
		return tp;
	}
	
	
	
}
