package see.naomi.beans;

import java.util.Date;

public class Comment {
	private String content;
	private String author;
	private Date created;
	private String email;
	private boolean published;
	private BlogPost blogPost;
	
	public Comment(String content, String author, String email) {
		this.content = content;
		this.author = author;
		this.email = email;
		created = new Date();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}
	
	
}