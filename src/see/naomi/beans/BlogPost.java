package see.naomi.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class BlogPost {
	private String content;
	private Date created;
	private Date updated;
	private String title;
	private boolean draft = true;
	
	/**
	 * List of comments currently loaded
	 */
	private List<Comment> comments = new LinkedList<Comment>();
	private int loadedCommentCount;
	private int numberOfComments;
	/**
	 * List of comma-separated tags
	 */
	private String tags;

	public BlogPost() {
	}
	
	public static List<BlogPost> createBlogPost(ResultSet rs) {
		List<BlogPost> list = new ArrayList<BlogPost>();
		try {
			while (rs.next()) {
				Long id = rs.getLong("id");
				String title = rs.getString("title");
				String content = rs.getString("content");
				Date created = rs.getDate("created");
				Date updated = rs.getDate("updated");
				String temp = rs.getString("draft");
				boolean draft = (temp == null || temp.equals("Y"));
				BlogPost post = new BlogPost();
				post.setTitle(title);
				post.setContent(content);
				post.setCreated(created);
				post.setUpdated(updated);
				post.setDraft(draft);
				list.add(post);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return list;
	}
	
	public BlogPost(String title, String content, String tags) {
		this.title = title;
		this.content = content;
		this.tags = tags;
		created = new Date();
	}
	
	public void addComment(Comment comment) {
		//add comment to database here
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public int getLoadedCommentCount() {
		return loadedCommentCount;
	}
	public void setLoadedCommentCount(int loadedCommentCount) {
		this.loadedCommentCount = loadedCommentCount;
	}
	public int getNumberOfComments() {
		return numberOfComments;
	}
	public void setNumberOfComments(int numberOfComments) {
		this.numberOfComments = numberOfComments;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	
}
